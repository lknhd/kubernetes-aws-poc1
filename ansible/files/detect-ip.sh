#!/bin/bash

set -e

ip_addr=`ip address | grep 172.16. | awk '{print $2}'`
echo "Debian GNU/Linux 9 \n \l
IP Address: $ip_addr
" > /etc/issue

